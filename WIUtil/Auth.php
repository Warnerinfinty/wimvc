<?php
/**
 * 
 */
class Auth
{
    
    public static function handleLogin()
    {
                //if $_SESSION['user_id'] is not set return false
        $session = Session::get("user_id");
        if( $session == null)
             return false;
        
        //if enabled, check fingerprint
        if(LOGIN_FINGERPRINT == true) {
            $loginString  = self::_generateLoginString();
            $currentString = Session::get("login_fingerprint");
            if($currentString != null && $currentString == $loginString)
                
                return true;
            
            else  {
                //destroy session, it is probably stolen by someone
                self::logout();

            header('Location: index.php');
            die();
                
            }
        }
       
    }


        public static function AdminhandleLogin()
    {
                //if $_SESSION['user_id'] is not set return false
        $session = Session::get('role');
        if( $session >= "5")
             return true;
        
        //if enabled, check fingerprint
        if(LOGIN_FINGERPRINT == true) {
            $loginString  = self::_generateLoginString();
            $currentString = Session::get("login_fingerprint");
            if($currentString != null && $currentString == $loginString)
                
                return true;
            
            else  {
                //destroy session, it is probably stolen by someone
                self::logout();

            header('Location: ../index.php');
            die();
                
            }
        }
       
    }

        public static function logout() 
    {
        Session::destroySession();
    }

    private static function _generateLoginString() 
    {
        $userIP = $_SERVER['REMOTE_ADDR'];
        $userBrowser = $_SERVER['HTTP_USER_AGENT'];
        $loginString = hash('sha512',$userIP.$userBrowser);
        return $loginString;
    }
    
}