<div class="container-fluid text-center">    
  <div class="row content">

	<div class="col-lg-12 col-md-12 col-sm-12" >
						<div class="col-lg-12 col-md-12 col-sm-12" >

						 <div class="col-lg-12 col-md-12 col-sm-12 text-left"> 
							<div class="intro_box">
<h1><?php $this->Lang->get("welcome_"); ?><span></span></h1>
							<p><?php $this->Lang->get("main_title"); ?></p>
							</div>
						</div>
					</div>
				
					<div class="col-lg-4 col-md-4 col-sm-4" >
						<div class="services">
							<div class="icon">
								<i class="fa fa-laptop"></i>
							</div>
							<div class="serv_detail">
								<h3><?php $this->Lang->get("community"); ?></h3>
								<p><?php $this->Lang->get("learn"); ?>
</p>
							</div>
						</div>
					</div>
					
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="services">
							<div class="icon">
								<i class="fa fa-trophy"></i>
							</div>
							<div class="serv_detail">
								<h3><?php $this->Lang->get("software"); ?></h3>

							</div>
						</div>
					</div>
					
					<div class="col-lg-4 col-md-4 col-sm-4" >
						<div class="services">
							<div class="icon">
								<i class="fa fa-cogs"></i>
							</div>
							<div class="serv_detail">
								<h3><?php $this->Lang->get("it"); ?></h3>
								<p><?php $this->Lang->get("it_title") ; ?>
</p>
							</div>
						</div>
					</div>
					</div></div>
			</div>