 <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
<form class="form-horizontal" action="login/run" method="post">
                    <fieldset>
                      <div id="legend">
                        <legend class=""><?php $this->Lang->get("login"); ?></legend>
                      </div>    
                      <div class="control-group form-group">
                        <!-- Username -->
                        <label class="control-label col-lg-4"  for="login-username"><?php $this->Lang->get("username") ; ?></label>
                        <div class="controls col-lg-8">
                          <input type="text" id="login-username" name="username" placeholder="" class="input-xlarge form-control"> <br />
                        </div>
                      </div>

                      <div class="control-group form-group">
                        <!-- Password-->
                        <label class="control-label col-lg-4" for="login-password"><?php $this->Lang->get("password") ; ?></label>
                        <div class="controls col-lg-8">
                          <input type="password" id="login-password" name="password" placeholder="" class="input-xlarge form-control">
                        </div>
                      </div>
 
                      
                      <div class="control-group form-group">
                        <!-- Password-->
                        <label class="control-label col-lg-4" for="login-password"></label>
                        <div class="controls col-lg-8">
                          <a href="forgot_password" class="btn"><?php $this->Lang->get("forgotten_password") ; ?></a>
                        </div>
                      </div>

                      <div class="control-group form-group">
                        <!-- Button -->
                        <div class="controls col-lg-offset-4 col-lg-8">
                          <button id="btn-login" class="btn btn-success"><?php $this->Lang->get("login") ; ?></button>
                        </div>
                      </div>
                    </fieldset>
                  </form>   
                  </div>             