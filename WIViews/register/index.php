 <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
 <form class="form-horizontal register-form" id="tab" action="register/run" method="post">
                      <fieldset>
                        <div id="legend">
                          <legend class=""><?php echo $this->Lang->get('create_account'); ?> </legend>
                        </div>

                        <div class="control-group  form-group">
                            <label class="control-label col-lg-4" for="reg-email" ><?php echo $this->Lang->get('email'); ?><span class="required">*</span></label>
                            <div class="controls col-lg-8">
                                <input type="text" id="reg-email" class="input-xlarge form-control" name="email">
                            </div>
                        </div>

                        <div class="control-group  form-group">
                            <label class="control-label col-lg-4" for="reg-username"><?php echo $this->Lang->get('username'); ?><span class="required">*</span></label>
                            <div class="controls col-lg-8">
                                <input type="text" id="reg-username" class="input-xlarge form-control" name="username">
                            </div>
                        </div>


                        <div class="control-group  form-group">
                            <label class="control-label col-lg-4" for="reg-password"><?php echo $this->Lang->get('password'); ?><span class="required">*</span></label>
                            <div class="controls col-lg-8">
                                <input type="password" id="reg-password" class="input-xlarge form-control" name="password">
                            </div>
                        </div>

                        <div class="control-group  form-group">
                            <label class="control-label col-lg-4" for="reg-repeat-password"><?php echo $this->Lang->get('repeat_password'); ?><span class="required">*</span></label>
                            <div class="controls col-lg-8">
                                <input type="password" id="reg-repeat-password" class="input-xlarge form-control" name="repeat_pass">
                            </div>
                        </div>

                        

                          
                        <div class="control-group  form-group">
                            <label class="control-label col-lg-4" for="reg-bot-sum">
            <?php echo Session::get("bot_first_number")  ?>+ 
                                <?php echo Session::get("bot_second_number")  ?>
                                <span class="required">*</span>
                            </label>
                            <div class="controls col-lg-8">
                                <input type="text" id="reg-bot-sum" class="input-xlarge form-control" name="botProtection">
                            </div>
                        </div>

                        <div class="control-group  form-group">
                            <div class="controls col-lg-offset-4 col-lg-8">
                                <button id="btn-register" class="btn btn-success" type="submit"><?php echo $this->Lang->get('create_Account'); ?></button>
                            </div>
                        </div>

                        <div class="control-group  form-group">
                            <div class="controls col-lg-offset-4 col-lg-8">

                            </div>
                        </div>
                       </fieldset>
                  </form>
                </div>
                
                 