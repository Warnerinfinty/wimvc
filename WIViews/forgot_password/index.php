 <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
 	 <form class="form-horizontal" id="forgot-pass-form" action="forgot_password/run" method="post">
                        <fieldset>
                          <div id="legend">
                            <legend class=""><?php $this->Lang->get("forgot_password"); ?></legend>
                          </div>    
                          <div class="control-group form-group">
                            <!-- Username -->
                            <label class="control-label col-lg-4"  for="forgot-password-email"><?php $this->Lang->get("your_email") ;?></label>
                            <div class="controls col-lg-8">
                              <input type="email" id="forgot-password-email" class="input-xlarge form-control" name="userEmail">
                            </div>
                          </div>

                          <div class="control-group form-group">
                            <!-- Button -->
                            <div class="controls col-lg-offset-4 col-lg-8">
                              <button id="btn-forgot-password" class="btn btn-success"><?php $this->Lang->get("reset_password");?></button>
                            </div>
                          </div>
                        </fieldset>
                      </form>
</div>