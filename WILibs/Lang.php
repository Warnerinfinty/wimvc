<?php

class Lang extends WI
{

    public function __construct() {
        parent::__construct();
    }

    public function  all($jsonEncode = true) 
    {
        // determine lanuage

	    //$lang = new self();
		$language = $this->getLanguage();

		//echo $language;
		//$WIdb = WIdb::getInstance();
			//$file = WILang::getFile($language);
			//echo $file;
			//echo $language;
		if ( ! self::isValidLanguage($language) )
		die('Language file doesn\'t exist!');
		else {

			$lang = $language[0];
			//echo $lang;

			        $result = $this->WIdb->select(
                    "SELECT * FROM `wi_lang` WHERE `lang` = :file",
                     array(
                       "file" => $lang
                     )
                  );
/*		$sth = $this->WIdb->prepare("SELECT * FROM `wi_lang` WHERE `lang` = :file");
        $sth->execute(array(':file' => $lang));
	   while ($result = $sth->fetchAll(PDO::FETCH_ASSOC)) {*/
				echo "{";
				foreach ($result as $res) {
				echo '"' .$res['keyword'] .'":"' . $res['translation'] . '",';
				 //return array($res['keyword'] => $res['translation'] ,);	
			}

			echo "}";
			}

			//}
	}

	    

    public function get($key ) //, $bindings = array()
    {
		// determine language
		$language = $this->getLanguage();

		//$lang = $language[0];
		//echo "lang ".$language;
		//echo "k".$key;
		$result = $this->WIdb->select(
                    "SELECT `translation` FROM `wi_trans`
                     WHERE `keyword` = :k AND `lang` = :l",
                     array(
                       "k" => $key,
                       "l" => $language
                     )
                  );

		//var_dump($result);
		
		//echo "trans".$trans;
		if($result > 0){
			$trans = $result[0]['translation'];
			echo $trans;
		}else{
			return '';
		}
		
	}

	    public function langClassSelector($lang)
    {
      //echo $lang;

      if($this->getLanguage() === $lang){
        return $this->getLanguage();
      }else{
        return "fade";
      }

    }

	      public function viewLang()
    {
    

         
        $sql = "SELECT * FROM `wi_lang`";
        $query = $this->WIdb->prepare($sql);
        $query->execute();
         echo '<div class="flags-wrapper">';
        while($res = $query->fetchAll(PDO::FETCH_ASSOC) ){

          
        foreach ($res as $lang ) {

            echo '<a href="' . $lang['href'] . '">
                 <img src="'.SCRIPT_URL.'/WIPublic/images/lang/' . $lang['lang_flag'] . '" alt="' . $lang['name'] .'" title="' . $lang['name'] .'"
                      class="'. $this->langClassSelector($lang['lang']) .'" /></a>';
            }
        }

         echo '</div>';
    }


         public static function changeLanguage($language) 
     {

     		
			//set language cookie to 1 year
			setcookie('wi_lang', $language, time() + 60 * 60 * 24 * 365, '/');

            // update session
			Session::set('wi_lang', $language);

            //refresh the page
            //$url0 =  $_SERVER['REQUEST_URI'];
            
            $file = basename($_SERVER['REQUEST_URI']); 

            $uri = trim(strtok($file, '?'));

            $url = $uri;
            //echo $url;
			    if ( ! headers_sent() )
			    {    
			       header('Location: '.$url, TRUE, 302);
			        exit;
			    }
			    else
			    {
		        echo '<script type="text/javascript">';
		       echo 'window.location.href="'.$url.'";';
		        echo '</script>';
		        echo '<noscript>';
		        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
		        echo '</noscript>';
		        exit;
		    }
			//echo Session::get('wi_lang');

		
	}

     public static function setLanguage() 
     {
     		if ($language = self::isValidLanguage($language) ) {
			//set language cookie to 1 year
			setcookie('wi_lang', $language, time() + 60 * 60 * 24 * 365, '/');

            // update session
			Session::set('wi_lang', $language);
			//echo Session::get('wi_lang');
		}
  		
	}

		 public  function getLanguage() 
		 {
        // check if cookie exist and language value in cookie is valid
        if ( isset ( $_COOKIE['wi_lang'] ) && self::isValidLanguage ( $_COOKIE['wi_lang'] ) )
            return $_COOKIE['wi_lang']; // return lang from cookie
        else
            return Session::get('wi_lang', DEFAULT_LANGUAGE);
    }


        public  function getTrans($language) 
        {
			//$file = WILang::getFile($language);
			//echo $file;
			//echo $language;
		if ( ! self::isValidLanguage($language) )
		echo 'Language file doesn\'t exist!';  // die('');
		else {
			//$language = include $file;
			//return $language;
			$lang = $language[0];

	    $sth = $this->WIdb->prepare("SELECT * FROM `wi_trans` WHERE `lang` = :file");
        $sth->execute(array(
            ':file' => $lang        ));
        
		while ($data = $sth->fetchAll(PDO::FETCH_ASSOC)) {
			if(empty($data)){
				//echo "no data";
			}else{
								echo "{";
				foreach ($data as $res) {
				echo '"' .$res['keyword'] .'":"' . $res['translation'] . '",';
				 //return array($res['keyword'] => $res['translation'] ,);	
			}

			echo "}";
			}

			}
			}

			
	}



	 private function getFile($language) 
	 {
	 	//echo $language;
	 	//var_dump($language);
	 	$lang = $language[0];
	 	//echo "lang" . $language;
	 	if (empty($lang)) {
	 		return '';
	 	}else{
	 		$sth = $this->WIdb->prepare("SELECT * FROM `wi_lang` WHERE `lang` = :file");
        $sth->execute(array(':file' => $lang));
        $res = $sth->fetch();
 
		if ($res > 0)
			return $res['lang'];
		}

		
	}

	    private static function isValidLanguage($lang) 
	    {

	    $Lang = new self();
		
		$file = $Lang->getFile($lang);
		//echo $file;

		if($file == ""){
			$file = DEFAULT_LANGUAGE;
			return $file;
		}else{
			return $file;
		}

	}





}