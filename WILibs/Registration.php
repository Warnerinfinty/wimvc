<?php
class Registration extends WI
{
    
	    public function __construct() {
        parent::__construct();
       // $this->Boot = new Bootstrap();

    }

    public function botProtection()
     {
        Session::set("bot_first_number", rand(1,9));
        Session::set("bot_second_number", rand(1,9));
    }


       /**
     * Hash given password.
     * @param string $password Unhashed password.
     * @return string Hashed password.
     */
     public function hashPassword($password)
     {
        //this salt will be used in both algorithms
        
        //echo "heyM";
        $salt = "$2a$" . PASSWORD_BCRYPT_COST . "$" . PASSWORD_SALT;
        //echo "enc " . PASSWORD_ENCRYPTION;
        if(PASSWORD_ENCRYPTION == "bcrypt") {
//for bcrypt it is required to look like this,
            //echo "hey";
            $newPassword = crypt($password, $salt);
            //echo "newP". $newPassword;
        }
        else {
            //for sha512 it is not required but it can be used 
            $newPassword = $password;
            for($i=0; $i<PASSWORD_SHA512_ITERATIONS; $i++)
                $newPassword = hash('sha512',$salt.$newPassword.$salt);
        }
        //echo "newpas" . $newPassword;
        return $newPassword;
     }


         private function _generateKey()
    {
        return md5(time() . PASSWORD_SALT . time());
    }


    
}