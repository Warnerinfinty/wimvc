<?php

class Website extends WI
{

    public function __construct() {
        parent::__construct();
        //$this->Session = new Session();
        //$this->Model = new Model();
        //$this->WIdb = WIdb::getInstance();
    }

    public function pageCss($page)
    {
    	            $result = $this->WIdb->select(
                    "SELECT * FROM `wi_css`
                     WHERE `page` = :p",
                     array(
                       "p" => $page
                     )
                  );
    	            //var_dump($result);
    	           foreach ($result as $res) {
    	           	echo '<link rel="stylesheet" href="' .SCRIPT_URL . 'WIPublic/WITheme/' .THEME. '/css/' . $res['href'] . '">';
    	           }
    }

        public function pageJs($page)
    {
    	            $result = $this->WIdb->select(
                    "SELECT * FROM `wi_scripts`
                     WHERE `page` = :p",
                     array(
                       "p" => $page
                     )
                  );
    	            //var_dump($result);
    	           foreach ($result as $res) {
    	           	echo '<script type="text/javascript" src="' .SCRIPT_URL . '/WIPublic/WITheme/' .THEME . '/js/' . $res['src'] . '"></script>';
    	           }
    }

        public function pageMeta($page)
    {
    	            $result = $this->WIdb->select(
                    "SELECT * FROM `wi_meta`
                     WHERE `page` = :p",
                     array(
                       "p" => $page
                     )
                  );
    	            //var_dump($result);
    	           foreach ($result as $res) {
    	           	//var_dump($res);
    	           	echo '<meta name="' . $res['name'] . '" content="' . $res['content'] . '" author="' . $res['author'] . '" >';
    	           }
    }



    /* admin functions */

        public function AddChildren($id)
    {

            $result = $this->WIdb->select(
                    "SELECT * FROM `wi_sidebar`
                     WHERE `parent` = :id",
                     array(
                       "id" => $id
                     )
                  );
        
        foreach ($result as $res) {

            echo '<li><a href="' .SCRIPT_URL . 'admin/' . $res['link'] . '"><i class="fa fa-angle-double-right"></i>
            <img class="img-responsive mobileShow" src="WIMedia/Img/icons/admin_sidebar/' . $res['img'] . '.png">
            <span class="mobileHide">' . $res['lang'] . '</span></a></li>';
        }

    }

        public function EditAddChildren($id)
    {

        $result = $this->WIdb->select(
                    "SELECT * FROM `wi_sidebar`
                     WHERE `parent` = :id",
                     array(
                       "id" => $id
                     )
                  );
        
        foreach ($result as $res) {
            echo '<li><input type="text" id="sidebar_href"  maxlength="88" name="sidebar_href" placeholder="Sidebar href" class="input-xlarge form-control" value="' . $res['link'] . '"> <br />
            <i class="fa fa-angle-double-right"></i>
            <input type="text" id="sidebar_menu"  maxlength="88" name="sidebar_menu" placeholder="Sidebar menu" class="input-xlarge form-control" value="' . $res['lang'] . '"> <br />
            </li>';
        }

    }

         public function MainHeader()
    {
        $sql = "SELECT * FROM `wi_header`";
        $query = $this->WIdb->prepare($sql);
        $query->execute();

        while($res = $query->fetch(PDO::PARAM_STR))
        {
         echo ' <header class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-2">
                            <div class="navbar_brand">
                            <div id="HeaderImg">
                 <img class="img-responsive cp" id="headerPic" src="WIMedia/Img/header/'. $res['logo'] . '" style="width:120px; height:120px;"></div>
                    <button class="btn mediaPic" onclick="WIMedia.changePic()">Change Picture</button>
                    </div>
                            </div>
                        </div>
                </div> 
        </header>';
      }

    }



    public function EditAdminSideBar()
    {

        $result = $this->WIdb->select(
                    "SELECT * FROM `wi_sidebar`"
                  );
        
        $menu_order = $result['sort'];

        $result1 = $this->WIdb->select(
                    "SELECT * FROM  `wi_sidebar` a LEFT OUTER JOIN (SELECT parent, COUNT( * ) AS Count
        FROM  `wi_sidebar` GROUP BY parent)Deriv1 ON a.id = Deriv1.parent",
                     array(
                       "order" => $menu_order
                     )
                  );
        echo '<ul class="sidebar-menu">
                       
                        </ul>
                <div id="editaccordion">';

        foreach ($result1 as $res) {
        if($res['parent'] > 0) 
        {
                echo '<h3> <input type="text" id="sidebar_label"  maxlength="88" name="sidebar_label" placeholder="Sidebar Label" class="input-xlarge form-control" value="' . $res['label'] . '"> <br />
                </h3><div>';
                Website::EditAddChildren($res['id']);
                echo '</div>';
                }
        }
        echo ' </div>';
    }


    public function AdminSideBar()
    {

        $result = $this->WIdb->select(
                    "SELECT * FROM `wi_sidebar`"
                  );
        //var_dump($result);
        $menu_order = $result[0]['sort'];

        $result1 = $this->WIdb->select(
                    "SELECT * FROM  `wi_sidebar` a LEFT OUTER JOIN (SELECT parent, COUNT( * ) AS Count
        FROM  `wi_sidebar` GROUP BY parent)Deriv1 ON a.id = Deriv1.parent",
                     array(
                       "order" => $menu_order
                     )
                  );
        echo '<ul class="sidebar-menu">
                        <li class="active">
                            <a href="dashboard.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        </ul>
                <div id="accordion">';

        foreach ($result1 as $res) {        { 
        if($res['parent'] > 0) 
            {
                echo '<h3>' . $res['label'] . '</h3><div>';
                Website::AddChildren($res['id']);
                echo '</div>';
            }


        }
        
        }
        echo ' </div>';

    }


    public function Website_Info($column) 
  {


    $user_id = 1;

            $result = $this->WIdb->select(
                    "SELECT * FROM `wi_site` WHERE `id` = :user_id",array(
                      "user_id" => $user_id
                    )
                  );
   // var_dump($result[0]);
    return $result[0][$column];
  }


    public function MainMenu()
    {
                  $result = $this->WIdb->select(
                    "SELECT * FROM `wi_menu`"
                  );

        $menu_order = $result[0]['sort'];

        $result0 = $this->WIdb->select(
                    "SELECT * FROM `wi_menu` ORDER BY :order",array(
                      "order" => $menu_order
                    )
                  );

        echo '<div class="col-lg-9 col-md-9 col-sm-12 menu">
              <div id="nav">
               <ul id="mainMenu" class="mainMenu default">';

          foreach ($result0 as $res ){    
         echo '<li> <input type="text" id="menu_name"  maxlength="88" name="menu_name" placeholder="menu name" class="input-xlarge form-control" value="' . $this->Lang->get('' .$res['lang'] .'') . '"></li>';
         if($res['parent'] > 0)
         {
            echo '<li><a href="' . $res['link'] . '">' . $this->Lang->get('' .$res['lang'] .'') . '</a></li>';
         }
        }
        echo '</ul>
            </div><!-- nav -->   
            <!-- end of menu -->
            </div>';
      }  
    
}