<?php
class Session
{
    
    public static function init()
    {
        //echo SESSION_SECURE;
        //$sessionCookies = SESSION_USE_ONLY_COOKIES;
        //echo "cookies" . $sessionCookies;
        //echo $cookie;
         if(SESSION_USE_ONLY_COOKIES === "true"){
        ini_set('session.use_only_cookies', SESSION_USE_ONLY_COOKIES);
        }

        if(SESSION_HTTP_ONLY === "true"){
        ini_set( 'session.cookie_httponly', 1 );
        }
        
        
        if(SESSION_SECURE === "true"){
        $cookieParams = session_get_cookie_params();
        session_set_cookie_params(
            $cookieParams["lifetime"], 
            $cookieParams["path"], 
            $cookieParams["domain"], 
            SESSION_SECURE
         );
        }
        session_start();
        
    }
    
    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }
    
    public static function get($key)
    {
        if (isset($_SESSION[$key]))
        return $_SESSION[$key];
    }
    
    public static function destroy()
    {
        //unset($_SESSION);
        session_destroy();
    }

     public static function destroySession() {

        $_SESSION = array();

        $params = session_get_cookie_params();

        setcookie(  session_name(), 
                    '', 
                    time() - 42000, 
                    $params["path"], 
                    $params["domain"], 
                    $params["secure"], 
                    $params["httponly"]
                );

        session_destroy();
    }
    
}