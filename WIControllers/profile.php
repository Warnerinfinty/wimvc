<?php

class Profile extends Controller 
{

    function __construct() {
        parent::__construct();
        Auth::handleLogin();
        $this->Web = new Website();
        //$this->view->js = array('dashboard/js/default.js');
    }
    
    function index() 
    {    
        $this->view->Web = $this->Web;
        $this->view->title = 'Profile';
        $this->view->page = 'profile';
        
        $this->view->render('header');
        $this->view->render('profile/index');
        $this->view->render('footer');
    }
    
    function logout()
    {
        Session::destroy();
        header('location: ' . SCRIPT_URL .  'login');
        exit;
    }
    
    function xhrInsert()
    {
        $this->model->xhrInsert();
    }
    
    function xhrGetListings()
    {
        $this->model->xhrGetListings();
    }
    
    function xhrDeleteListing()
    {
        $this->model->xhrDeleteListing();
    }

}