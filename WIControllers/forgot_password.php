<?php

class forgot_password extends Controller 
{
    function __construct() {
        parent::__construct(); 
        $this->Lang = new Lang();
        $this->Web  = new Website();
        $this->Reg = new Registration();
        $this->Err = new ErrorHandler();
        $this->mailer = new Email();
        $this->maint = new Maintenace();
    }
    
    
    function index() 
    {    
        $this->view->Web = $this->Web;
        $this->view->Lang = $this->Lang;
        
        $this->view->page = 'forgot_password';
        $this->view->title = 'Forgot Passowrd';
        
        $this->view->render('header');
        $this->view->render('forgot_password/index');
        $this->view->render('footer');
    }
    
    function run()
    {
        $this->model->maint = $this->maint;
        $this->model->Reg = $this->Reg;
        $this->model->Err = $this->Err;
        $this->model->mailer = $this->mailer;
        $this->model->run();
    }
    

}