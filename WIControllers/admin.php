<?php

class Admin extends Controller 
{

    function __construct() {
        parent::__construct();
        Auth::AdminhandleLogin();
        $this->Web = new Website();
        $this->AdminChat = new AdminChat();
        $this->Dash = new Dashboard();
        //$this->view->js = array('dashboard/js/default.js');
        //echo "string";
    }
    
    
    function index() 
    {
        $this->view->Dash = $this->Dash;
        $this->view->AdminChat = $this->AdminChat;    
        $this->view->Web = $this->Web;
        $this->view->title = 'Dashboard';
        $this->view->page = 'dashboard';
        
        $this->view->render('header');
        $this->view->render('admin/index');
        //$this->view->render('footer');
    }
    
    function logout()
    {
        Session::destroy();
        header('location: ' . SCRIPT_URL .  'login');
        exit;
    }
    
    function xhrInsert()
    {
        $this->model->xhrInsert();
    }
    
    function xhrGetListings()
    {
        $this->model->xhrGetListings();
    }
    
    function xhrDeleteListing()
    {
        $this->model->xhrDeleteListing();
    }

}