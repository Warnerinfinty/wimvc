<?php

class Login extends Controller 
{
    function __construct() {
        parent::__construct(); 
        $this->Lang = new Lang();
        $this->Web  = new Website();
        $this->Reg = new Registration();
        $this->Err = new ErrorHandler();
        $this->maint = new Maintenace();
        $this->mailer = new Email();
    }
    
    
    function index() 
    {    
        $this->view->Web = $this->Web;
        $this->view->Lang = $this->Lang;
        
        $this->view->page = 'login';
        $this->view->title = 'Login';
        
        $this->view->render('header');
        $this->view->render('login/index');
        $this->view->render('footer');
    }
    
    function run()
    {
        $this->model->mailer = $this->mailer;
        $this->model->maint = $this->maint;
        $this->model->Reg = $this->Reg;
        $this->model->Err = $this->Err;
        $this->model->run();
    }
}