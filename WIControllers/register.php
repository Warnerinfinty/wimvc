<?php

class Register extends Controller
{

    function __construct() {
        parent::__construct();
        $this->Lang = new Lang();
        $this->Web  = new Website();
        $this->mailer = new Email();
        $this->maint = new Maintenace();
        $this->valid = new Validate();
        $this->Auth = new Auth();
    }
    
    
    function index() 
    {   
        $this->model->botProtection();
        $this->view->Web = $this->Web;
        $this->view->Lang = $this->Lang;
        $this->view->page = 'register';
        $this->view->title = 'Register';
        
        $this->view->render('header');
        $this->view->render('register/index');
        $this->view->render('footer');
    }
    
    function run()
    {
        $this->model->Lang = $this->Lang;
        $this->model->maint = $this->maint;
        $this->model->mailer = $this->mailer;
        $this->model->valid = $this->valid;
        $this->model->run();

    }
    


}