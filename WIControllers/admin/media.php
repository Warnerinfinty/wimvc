<?php

class Media extends Controller 
{

    function __construct() {
        parent::__construct();
        Auth::AdminhandleLogin();
        $this->Web = new Website();
        $this->Dash = new Dashboard();
        $this->Img  = new Image();
        $this->Vid  = new Video();
        //$this->view->js = array('dashboard/js/default.js');
        //echo "string";
    }
    
    
    function index() 
    {
        $this->view->Img  = $this->Img;
        $this->view->Vid = $this->Vid;
        $this->view->Dash = $this->Dash;
        $this->view->Web = $this->Web;
        $this->view->title = 'Media';
        $this->view->page = 'dashboard';
        
        $this->view->render('header');
        $this->view->render('admin/media/index');
        //$this->view->render('footer');
    }
    
    function logout()
    {
        Session::destroy();
        header('location: ' . SCRIPT_URL .  'login');
        exit;
    }
    
    function xhrInsert()
    {
        $this->model->xhrInsert();
    }
    
    function xhrGetListings()
    {
        $this->model->xhrGetListings();
    }
    
    function xhrDeleteListing()
    {
        $this->model->xhrDeleteListing();
    }

}