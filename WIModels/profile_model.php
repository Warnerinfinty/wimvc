<?php

class Profile_Model extends Model 
{

    public function __construct() {
        parent::__construct();
    }
    
    public function xhrInsert() 
    {
        $text = $_POST['text'];
        
        $this->WIdb->insert('data', array('text' => $text));
        
        $data = array('text' => $text, 'id' => $this->WIdb->lastInsertId());
        echo json_encode($data);
    }
    
    public function xhrGetListings()
    {
        $result = $this->WIdb->select("SELECT * FROM data");
        echo json_encode($result);
    }
    
    public function xhrDeleteListing()
    {
        $id = (int) $_POST['id'];
        $this->WIdb->delete('data', "id = '$id'");
    }

}