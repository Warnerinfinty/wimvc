<?php

class Login_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
//hash password and get data from WIdb
        $password = strip_tags( trim( $_POST['password'] ) );
        $username = strip_tags( trim( $_POST['username'] ) );
        $pass = $this->_hashPassword($password);
        //var_dump($username);
        //echo "past".$password;
        //echo $_POST['username'];
        //echo $_POST['password'];
        
        $result = $this->WIdb->select(
                    "SELECT * FROM `wi_members`
                     WHERE `username` = :u AND `password` = :p",
                     array(
                       "u" => $username,
                       "p" => $pass
                     )
                  );
        //var_dump($result);
    
        if(count($result) == 1) 
        {
            if( MAIL_CONFIRMATION_REQUIRED === "true"){
                 if($result[0]['confirmed'] == "N") {
                $this->Err->Code(array(
                    'status' => 'error',
                    'message' => $this->Lang::get('user_not_confirmed')
                ));

            }
               
            }

            // check if user is banned
            if($result[0]['banned'] == "Y") {
                // increase attempts to prevent touching the DB every time
                $this->increaseLoginAttempts();

                    $this->Err->Code(array(
                    'status' => 'error',
                    'message' => $this->Lang::get('user_banned')
                ));

            }

            //user exist, log him in if he is confirmed
            $this->_updateLoginDate($result[0]['user_id']);
            Session::set("user_id", $result[0]['user_id']);
            Session::set("role", $result[0]['user_role']);

            if(LOGIN_FINGERPRINT == true)
                Session::set("login_fingerprint", $this->_generateLoginString ());
            $st1  = $_POST['username'] ;
            $st2  = "Successfully logged in User:" .$_POST['username']; ;
            $this->maint->LogFunction($st1, $st2);
            //echo "log in" . Session::get("user_id");
            $page = "profile";
            //echo SCRIPT_URL . $page;

            $url = rtrim(SCRIPT_URL, '/') . '/' . ltrim($url, '/');

            if ( ! headers_sent() )
            {    
                header('Location: '.$url.'profile', TRUE, 302);
                exit;
            }
            else
            {
                echo '<script type="text/javascript">';
                echo 'window.location.href="'.$url.'profile";';
                echo '</script>';
                echo '<noscript>';
                echo '<meta http-equiv="refresh" content="0;url='.$url.'profile" />';
                echo '</noscript>';
                exit;
            }
        }
        return false;
        echo 'incorrect username/password';

        
    }


        /**
     * Increase login attempts from specific IP address to preven brute force attack.
     */
    public function increaseLoginAttempts() {
        $date    = date("Y-m-d");
        $user_ip = $_SERVER['REMOTE_ADDR'];
        $table   = 'wi_login_attempts';
       
        //get current number of attempts from this ip address
        $loginAttempts = $this->_getLoginAttempts();
        echo $loginAttempts;
        echo $date;
        echo $user_ip;
        //if they are greater than 0, update the value
        //if not, insert new row
        if($loginAttempts > 0)
            $this->WIdb->update (
                        $table, 
                        array( 
                        "attempt_number" => $loginAttempts + 1 ), 
                        "`ip_addr` = :ip_addr AND `date` = :d", 
                        array( 
                            "ip_addr" => $user_ip,
                             "d" => $date
                         )
                      );
        else
            $this->WIdb->insert($table, array(
                "ip_addr" => $user_ip,
                "date"    => $date
            ));
    }


    /**
     * Log out user and destroy session.
     */
    public function logout() {
        Session::destroySession();
    }

      /**
     * Check if someone is trying to break password with brute force attack.
     * @return TRUE if number of attemts are greater than allowed, FALSE otherwise.
     */
    public function _isBruteForce()
    {
        return $this->_getLoginAttempts() > LOGIN_MAX_LOGIN_ATTEMPTS;
    }


        /* PRIVATE AREA
     =================================================*/

         private function _validateLoginFields($username, $password) {
        $id     = $_POST['id'];
        $errors = array();
        
        if($username == "")
            $errors[] = WILang::get('username_required');
        
        if($password == "")
            $errors[] = WILang::get('password_required');
        
        return $errors;
    }
    

        private function _generateLoginString() {
        $userIP = $_SERVER['REMOTE_ADDR'];
        $userBrowser = $_SERVER['HTTP_USER_AGENT'];
        $loginString = hash('sha512',$userIP.$userBrowser);
        return $loginString;
    }

        private function _getLoginAttempts() {
        $date = date("Y-m-d");
        $user_ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;

        if ( ! $user_ip )
            return PHP_INT_MAX;
        
         $query = "SELECT `attempt_number`
                   FROM `wi_login_attempts`
                   WHERE `ip_addr` = :ip AND `date` = :date";

        $result = $this->WIdb->select($query, array(
            "ip"    => $user_ip,
            "date"  => $date
        ));

        if(count($result) == 0)
            return 0;

        return intval($result[0]['attempt_number']);
    }

    private function _hashPassword($password) {
        //$register = new Register();
        $NewPAss = $this->Reg->hashPassword($password);
        //echo $NewPAss;
        return $NewPAss;
    }
    
    
    /**
     * Update database with login date and time when this user is logged in.
     * @param int $userid Id of user that is logged in.
     */
    private function _updateLoginDate($userid) {
        //echo $userid;
        $daTe = date("Y-m-d H:i:s");
        $this->WIdb->update(
                    "wi_members",
                    array("last_login" => $daTe,
                            "user_id" => $userid),
                    "user_id = :user_id"
                );
    }
    
}