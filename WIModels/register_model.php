<?php

class Register_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
        
    }


    public function botProtection()
     {
        Session::set("bot_first_number", rand(1,9));
        Session::set("bot_second_number", rand(1,9));
    }

    public function run()
    {

        
         //generate email confirmation key
            $key = $this->_generateKey();

          
            MAIL_CONFIRMATION_REQUIRED === true ? $confirmed = 'N' : $confirmed = 'Y';

           $this->WIdb->insert('wi_members', array(
            "email"     => $_POST['email'],
            "username"  => strip_tags($_POST['username']),
            "password"  => $this->hashPassword($_POST['password']),
            "confirmed" => $confirmed,
            "confirmation_key" => $key,
            "register_date" => date("Y-m-d"),
            "ip_addr" => getenv('REMOTE_ADDR')
        )); 

            $userId = $this->WIdb->lastInsertId();        
        
            $st1  = $_POST['username'] ;
            $st2  = "Added new user";
            //$maintain = new WIMaintenace();
            $this->maint->LogFunction($st1, $st2);

            $this->WIdb->insert('wi_user_details', array( 'user_id' => $userId ));
           // echo "mail" . MAIL_CONFIRMATION_REQUIRED;
            //send confirmation email if needed
            if ( MAIL_CONFIRMATION_REQUIRED === "true" ) {
                $this->mailer->confirmationEmail($_POST['email'], $key);
                $msg = $this->Lang->get('success_registration_with_confirm');

               // echo  WILang::get('success_registration_with_confirm');
            }
            else
                $msg = $this->Lang->get('success_registration_no_confirm');
            
            //prepare and output success message
/*            $result = array(
                "status" => "success",
                "msg"    => $msg
            );*/

                $url = rtrim(SCRIPT_URL, '/') . '/' . ltrim($url, '/');

            if ( ! headers_sent() )
            {    
                header('Location: '.$url.'login', TRUE, 302);
                exit;
            }
            else
            {
                echo '<script type="text/javascript">';
                echo 'window.location.href="'.$url.'login";';
                echo '</script>';
                echo '<noscript>';
                echo '<meta http-equiv="refresh" content="0;url='.$url.'login" />';
                echo '</noscript>';
                exit;
            }


            //echo json_encode($result);


        }
        


        /**
     * Hash given password.
     * @param string $password Unhashed password.
     * @return string Hashed password.
     */
     public function hashPassword($password)
     {
        //this salt will be used in both algorithms
        
        //echo "heyM";
        $salt = "$2a$" . PASSWORD_BCRYPT_COST . "$" . PASSWORD_SALT;
        //echo "enc " . PASSWORD_ENCRYPTION;
        if(PASSWORD_ENCRYPTION == "bcrypt") {
//for bcrypt it is required to look like this,
            //echo "hey";
            $newPassword = crypt($password, $salt);
            //echo "newP". $newPassword;
        }
        else {
            //for sha512 it is not required but it can be used 
            $newPassword = $password;
            for($i=0; $i<PASSWORD_SHA512_ITERATIONS; $i++)
                $newPassword = hash('sha512',$salt.$newPassword.$salt);
        }
        //echo "newpas" . $newPassword;
        return $newPassword;
     }


         private function _generateKey()
    {
        return md5(time() . PASSWORD_SALT . time());
    }
    
}