<?php

class User_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function userList()
    {
        return $this->WIdb->select('SELECT userid, login, role FROM user');
    }
    
    public function userSingleList($userid)
    {
        return $this->WIdb->select('SELECT userid, login, role FROM user WHERE userid = :userid', array(':userid' => $userid));
    }
    
    public function create($data)
    {
        $this->WIdb->insert('user', array(
            'login' => $data['login'],
            'password' => Hash::create('sha256', $data['password'], HASH_PASSWORD_KEY),
            'role' => $data['role']
        ));
    }
    
    public function editSave($data)
    {
        $postData = array(
            'login' => $data['login'],
            'password' => Hash::create('sha256', $data['password'], HASH_PASSWORD_KEY),
            'role' => $data['role']
        );
        
        $this->WIdb->update('user', $postData, "`userid` = {$data['userid']}");
    }
    
    public function delete($userid)
    {
        $result = $this->WIdb->select('SELECT role FROM user WHERE userid = :userid', array(':userid' => $userid));

        if ($result[0]['role'] == 'owner')
        return false;
        
        $this->WIdb->delete('user', "userid = '$userid'");
    }
}