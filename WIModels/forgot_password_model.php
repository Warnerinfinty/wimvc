<?php

class Forgot_password_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
            if($_POST['userEmail'] == "")
            return $this->Lang::get('email_required');
        if( $this->_isBruteForce() )
            return $this->Lang::get('brute_force');
        
        //ok, no validation errors, we can proceed

        //generate password reset key
        $key = $this->_generateKey();

        //write key to WIdb
        $this->WIdb->updating(
                    'wi_members',
                     array(
                         "password_reset_key" => $key,
                         "password_reset_confirmed" => 'N',
                         "password_reset_timestamp" => date('Y-m-d H:i:s')
                         
                     ),
                     "`email` = :email",
                     array(
                        "email" => $_POST['userEmail']
                    )
                );

        $this->increaseLoginAttempts();

        //send email
        $this->mailer->passwordResetEmail($_POST['userEmail'], $key);

        return TRUE;

        
    }


        /**
     * Increase login attempts from specific IP address to preven brute force attack.
     */
    public function increaseLoginAttempts() {
        $date    = date("Y-m-d");
        $user_ip = $_SERVER['REMOTE_ADDR'];
        $table   = 'wi_login_attempts';
       
        //get current number of attempts from this ip address
        $loginAttempts = $this->_getLoginAttempts();
        echo $loginAttempts;
        echo $date;
        echo $user_ip;
        //if they are greater than 0, update the value
        //if not, insert new row
        if($loginAttempts > 0)
            $this->WIdb->update (
                        $table, 
                        array( 
                        "attempt_number" => $loginAttempts + 1 ), 
                        "`ip_addr` = :ip_addr AND `date` = :d", 
                        array( 
                            "ip_addr" => $user_ip,
                             "d" => $date
                         )
                      );
        else
            $this->WIdb->insert($table, array(
                "ip_addr" => $user_ip,
                "date"    => $date
            ));
    }


    /**
     * Log out user and destroy session.
     */
    public function logout() {
        Session::destroySession();
    }

      /**
     * Check if someone is trying to break password with brute force attack.
     * @return TRUE if number of attemts are greater than allowed, FALSE otherwise.
     */
    public function _isBruteForce()
    {
        return $this->_getLoginAttempts() > LOGIN_MAX_LOGIN_ATTEMPTS;
    }


        /* PRIVATE AREA
     =================================================*/

         private function _validateLoginFields($username, $password) {
        $id     = $_POST['id'];
        $errors = array();
        
        if($username == "")
            $errors[] = WILang::get('username_required');
        
        if($password == "")
            $errors[] = WILang::get('password_required');
        
        return $errors;
    }
    

        private function _generateLoginString() {
        $userIP = $_SERVER['REMOTE_ADDR'];
        $userBrowser = $_SERVER['HTTP_USER_AGENT'];
        $loginString = hash('sha512',$userIP.$userBrowser);
        return $loginString;
    }

        private function _getLoginAttempts() {
        $date = date("Y-m-d");
        $user_ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;

        if ( ! $user_ip )
            return PHP_INT_MAX;
        
         $query = "SELECT `attempt_number`
                   FROM `wi_login_attempts`
                   WHERE `ip_addr` = :ip AND `date` = :date";

        $result = $this->WIdb->select($query, array(
            "ip"    => $user_ip,
            "date"  => $date
        ));

        if(count($result) == 0)
            return 0;

        return intval($result[0]['attempt_number']);
    }

    private function _hashPassword($password) {
        //$register = new Register();
        $NewPAss = $this->Reg->hashPassword($password);
        //echo $NewPAss;
        return $NewPAss;
    }
    
    
    /**
     * Update database with login date and time when this user is logged in.
     * @param int $userid Id of user that is logged in.
     */
    private function _updateLoginDate($userid) {
        //echo $userid;
        $daTe = date("Y-m-d H:i:s");
        $this->WIdb->update(
                    "wi_members",
                    array("last_login" => $daTe,
                            "user_id" => $userid),
                    "user_id = :user_id"
                );
    }


    private function _generateKey()
    {
        return md5(time() . PASSWORD_SALT . time());
    }
    
}