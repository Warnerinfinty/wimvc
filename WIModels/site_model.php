<?php

class Site_Model extends Model 
{

    public function __construct() {
        parent::__construct();
    }

    public function site_settings()
    {
        $site_id ="1";
       $website_name =  strip_tags( trim( $_POST['website_name'] ) );
       $website_domain = $_POST['website_domain'];
       $website_url = $_POST['website_url'];

        $table   = 'wi_site';

        $this->WIdb->update ($table, array( 
                "site_name" => $website_name,
                "site_domain" => $website_domain,
                "site_url" => $website_url,
                "id" => $site_id
                 ), 
                "`id` = :id "
                      );
        echo "successully updated";

        $url = rtrim(SCRIPT_URL, '/') . '/' . ltrim($url, '/');

            if ( ! headers_sent() )
            {    
                header('Location: '.$url.'profile', TRUE, 302);
                exit;
            }
            else
            {
                echo '<script type="text/javascript">';
                echo 'window.location.href="'.$url.'profile";';
                echo '</script>';
                echo '<noscript>';
                echo '<meta http-equiv="refresh" content="0;url='.$url.'profile" />';
                echo '</noscript>';
                exit;
            }
    }
    
    public function xhrInsert() 
    {
        $text = $_POST['text'];
        
        $this->WIdb->insert('data', array('text' => $text));
        
        $data = array('text' => $text, 'id' => $this->WIdb->lastInsertId());
        echo json_encode($data);
    }
    
    public function xhrGetListings()
    {
        $result = $this->WIdb->select("SELECT * FROM data");
        echo json_encode($result);
    }
    
    public function xhrDeleteListing()
    {
        $id = (int) $_POST['id'];
        $this->WIdb->delete('data', "id = '$id'");
    }

}